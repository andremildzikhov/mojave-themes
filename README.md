<div align="center">
<h1 align="center">
  <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/18689986/5a2ea75e7c9baff048263c60239e2665be1d.jpg" alt="Project">
  <br />
  Mojave Themes for Snaps
</h1>
</div>

<p align="center"><b>These are the Mojave GTK and icon themes for snaps</b>. Mojave is a Mac OSX like theme for GTK 3, GTK 2 and Gnome-Shell which supports GTK 3 and GTK 2 based desktop environments like Gnome, Pantheon, XFCE, Mate, etc.


This snap contains all flavours of the GTK and icon themes.

<b>Attributions</b>:
This snap is packaged from vinceliuice's <a href="https://github.com/vinceliuice/Mojave-gtk-theme">Mojave-gtk-theme</a> and <a href="https://github.com/vinceliuice/McMojave-circle">McMojave-circle</a>. The Mojave-theme and McMojave-circle themes are under a GPL3.0 licence.

</p>


## Install <a href="https://snapcraft.io/mojave-themes">
<a href="https://snapcraft.io/mojave-themes">
<img alt="Get it from the Snap Store" src="https://snapcraft.io/static/images/badges/en/snap-store-white.svg" />
</a>

You can install the theme from the Snap Store оr by running:

```
sudo snap install [theme_name]
```
To connect the theme to an app run:
```
sudo snap connect [other snap]:gtk-3-themes [theme_name]:gtk-3-themes 
```
```
sudo snap connect [other snap]:icon-themes [theme_name]:icon-themes
```
where [theme_name] is ```mojave-themes```

